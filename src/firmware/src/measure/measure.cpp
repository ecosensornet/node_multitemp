#include <Arduino.h>
#include <sensor_ds18b20.h>
#include <Wire.h>

#include "../settings.h"
#include "../calibration/calibration.h"

#include "measure.h"

DS18B20 ds(PIN_TH_DATA);

measure::Sample sample_cur;

void check_connection(uint8_t* sid){
    if (!ds.isConnected(sid)) {
#if (DEBUG == 1)
      Serial.print("No device found at address ");
      for (uint8_t j = 0; j < 8; j++) {
        Serial.print(sid[j]);
        Serial.print(", ");
      }
      Serial.println("end");
#endif // DEBUG
      while (true) {
        delay(10);
        board::signal_warning(1);
      }
    }
}


void measure::setup() {
  digitalWrite(PIN_CMD_VEXT_SENSOR, LOW);

#if (DEBUG == 1)
  Serial.println("List sensors");
  while (ds.selectNext()) {
    uint8_t address[8];
    ds.getAddress(address);

    Serial.print("Address: [");
    for (uint8_t i = 0; i < 8; i++) {
      Serial.print(", ");
      Serial.print(address[i]);
    }
    Serial.println("]");
  }
#endif // DEBUG

  // Check device present
  check_connection(calib.sensor0);
  check_connection(calib.sensor1);
  check_connection(calib.sensor2);

   digitalWrite(PIN_CMD_VEXT_SENSOR, HIGH);
}


void measure::sample(){
   digitalWrite(PIN_CMD_VEXT_SENSOR, LOW);

  for (uint8_t i=0; i < 2; i++) {
    delay(100);
    ds.select(calib.sensor0);
    sample_cur.t0 = ds.getTempC();
    ds.select(calib.sensor1);
    sample_cur.t1 = ds.getTempC();
    ds.select(calib.sensor2);
    sample_cur.t2 = ds.getTempC();
  }

   digitalWrite(PIN_CMD_VEXT_SENSOR, HIGH);
}
