#pragma once

#include "../settings.h"

namespace html_mode_lora {

String processor(const String& key);
void handle_html();
void handle_chg_mode();

}  // namespace html_mode_lora
