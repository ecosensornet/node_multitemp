#include <Arduino.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"
#include "html_sampling.h"
#include "mode_config.h"


String html_sampling::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return cfg.node_id;
  }

  return "Key not found";
}


void html_sampling::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to sampling.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    measure::sample();
    meas_cur = "";
    mode_config::fmt_meas(meas_cur);
  }

  server.process_and_send("/page_sampling.html", html_sampling::processor);
}

