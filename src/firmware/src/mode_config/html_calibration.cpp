#include <Arduino.h>

#include "../calibration/calibration.h"
#include "../config/config.h"

#include "html_calibration.h"
#include "mode_config.h"


String html_calibration::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return cfg.node_id;
  }
  else if (key == "sensor0_sid0") {
    return String(calib.sensor0[0]);
  }
  else if (key == "sensor0_sid1") {
    return String(calib.sensor0[1]);
  }
  else if (key == "sensor0_sid2") {
    return String(calib.sensor0[2]);
  }
  else if (key == "sensor0_sid3") {
    return String(calib.sensor0[3]);
  }
  else if (key == "sensor0_sid4") {
    return String(calib.sensor0[4]);
  }
  else if (key == "sensor0_sid5") {
    return String(calib.sensor0[5]);
  }
  else if (key == "sensor0_sid6") {
    return String(calib.sensor0[6]);
  }
  else if (key == "sensor0_sid7") {
    return String(calib.sensor0[7]);
  }
  else if (key == "sensor1_sid0") {
    return String(calib.sensor1[0]);
  }
  else if (key == "sensor1_sid1") {
    return String(calib.sensor1[1]);
  }
  else if (key == "sensor1_sid2") {
    return String(calib.sensor1[2]);
  }
  else if (key == "sensor1_sid3") {
    return String(calib.sensor1[3]);
  }
  else if (key == "sensor1_sid4") {
    return String(calib.sensor1[4]);
  }
  else if (key == "sensor1_sid5") {
    return String(calib.sensor1[5]);
  }
  else if (key == "sensor1_sid6") {
    return String(calib.sensor1[6]);
  }
  else if (key == "sensor1_sid7") {
    return String(calib.sensor1[7]);
  }
  else if (key == "sensor2_sid0") {
    return String(calib.sensor2[0]);
  }
  else if (key == "sensor2_sid1") {
    return String(calib.sensor2[1]);
  }
  else if (key == "sensor2_sid2") {
    return String(calib.sensor2[2]);
  }
  else if (key == "sensor2_sid3") {
    return String(calib.sensor2[3]);
  }
  else if (key == "sensor2_sid4") {
    return String(calib.sensor2[4]);
  }
  else if (key == "sensor2_sid5") {
    return String(calib.sensor2[5]);
  }
  else if (key == "sensor2_sid6") {
    return String(calib.sensor2[6]);
  }
  else if (key == "sensor2_sid7") {
    return String(calib.sensor2[7]);
  }

  return "Key not found";
}

void html_calibration::handle_POST() {
#if (DEBUG == 1)
    Serial.println("POST to calibration.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "sensor0_sid0") {
        calib.sensor0[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid1") {
        calib.sensor0[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid2") {
        calib.sensor0[2] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid3") {
        calib.sensor0[3] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid4") {
        calib.sensor0[4] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid5") {
        calib.sensor0[5] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid6") {
        calib.sensor0[6] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor0_sid7") {
        calib.sensor0[7] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid0") {
        calib.sensor1[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid1") {
        calib.sensor1[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid2") {
        calib.sensor1[2] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid3") {
        calib.sensor1[3] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid4") {
        calib.sensor1[4] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid5") {
        calib.sensor1[5] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid6") {
        calib.sensor1[6] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_sid7") {
        calib.sensor1[7] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid1") {
        calib.sensor2[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid2") {
        calib.sensor2[2] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid3") {
        calib.sensor2[3] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid4") {
        calib.sensor2[4] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid5") {
        calib.sensor2[5] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid6") {
        calib.sensor2[6] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_sid7") {
        calib.sensor2[7] = server.arg(i).toFloat();
      }
    }
    calibration::save_file();
}
