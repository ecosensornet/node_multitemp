#include <Arduino.h>

#include "../config/config.h"
#include "../measure/measure.h"
#include "../mode_espnow/fmt_meas.h"

#include "html_mode_espnow.h"
#include "mode_config.h"

String espnow_meas_cur = "none";

String html_mode_espnow::processor(const String& key) {
  if (key == "val_cur") {
    return espnow_meas_cur;
  }
  else if (key == "gate_ip0") {
    return String(cfg.gate_ip[0]);
  }
  else if (key == "gate_ip1") {
    return String(cfg.gate_ip[1]);
  }
  else if (key == "gate_ip2") {
    return String(cfg.gate_ip[2]);
  }
  else if (key == "gate_ip3") {
    return String(cfg.gate_ip[3]);
  }
  else if (key == "gate_ip4") {
    return String(cfg.gate_ip[4]);
  }
  else if (key == "gate_ip5") {
    return String(cfg.gate_ip[5]);
  }
  else if (key == "gateway_ip_hex") {
    char buffer[20];
    sprintf (buffer, "%02X-%02X-%02X-%02X-%02X-%02X",
            cfg.gate_ip[0], cfg.gate_ip[1], cfg.gate_ip[2], cfg.gate_ip[3], cfg.gate_ip[4], cfg.gate_ip[5]);
    return String(buffer);
  }
  else if (key == "interval") {
    return String(cfg.interval);
  }

  return "Key not found";
}


void html_mode_espnow::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving mode_espnow.html");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to mode_espnow.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    bool need_saving = false;
    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "gate_ip0") {
        cfg.gate_ip[0] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "gate_ip1") {
        cfg.gate_ip[1] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "gate_ip2") {
        cfg.gate_ip[2] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "gate_ip3") {
        cfg.gate_ip[3] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "gate_ip4") {
        cfg.gate_ip[4] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "gate_ip5") {
        cfg.gate_ip[5] = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "interval") {
        cfg.interval = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "sample") {
        measure::sample();
        uint8_t msg[256];
        uint8_t msg_len = mode_espnow::fmt_meas(msg);
        espnow_meas_cur = "[";
        for (uint8_t i=0; i <msg_len;i++) {
            espnow_meas_cur += String(msg[i]);
            espnow_meas_cur += ", ";
        }
        espnow_meas_cur += "]";
      }
    }
    if (need_saving) {
      config::save_file();
    }
  }
  server.process_and_send("/page_mode_espnow.html", html_mode_espnow::processor);
}

void html_mode_espnow::handle_chg_mode() {
#if (DEBUG == 1)
  Serial.println("handle_chg_mode_espnow");
#endif
  cfg_mode = MODE_ESPNOW;
  ESP.deepSleep(0);
}

