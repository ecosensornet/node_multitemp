#include <ArduinoJson.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


void mode_config::fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["t0"] = sample_cur.t0;
  doc["t1"] = sample_cur.t1;
  doc["t2"] = sample_cur.t2;

  serializeJsonPretty(doc, msg);
}

