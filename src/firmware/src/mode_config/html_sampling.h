#pragma once

#include "../settings.h"

namespace html_sampling {

String processor(const String& key);
void handle_html();

}  // namespace html_sampling
