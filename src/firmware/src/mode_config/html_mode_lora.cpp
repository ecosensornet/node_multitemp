#include <Arduino.h>

#include "../config/config.h"
#include "../measure/measure.h"
#include "../mode_lora/fmt_meas.h"

#include "html_mode_lora.h"
#include "mode_config.h"

String lora_meas_cur = "none";

String html_mode_lora::processor(const String& key) {
  if (key == "val_cur") {
    return lora_meas_cur;
  }
  else if (key == "interval") {
    return String(cfg.interval);
  }
  else if (key == "APPEUI") {
    char msg[25];
    for (uint8_t i = 0; i <8; i++) {
        sprintf(&msg[i*3], "%02X ", cfg.APPEUI[7 - i]);
    }
    return String(msg);
  }
  else if (key == "DEVEUI") {
    char msg[25];
    for (uint8_t i = 0; i <8; i++) {
        sprintf(&msg[i*3], "%02X ", cfg.DEVEUI[7 - i]);
    }
    return String(msg);
  }
  else if (key == "APPKEY") {
    char msg[49];
    for (uint8_t i = 0; i <16; i++) {
        sprintf(&msg[i*3], "%02X ", cfg.APPKEY[i]);
    }
    return String(msg);
  }

  return "Key not found";
}


void html_mode_lora::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving mode_lora.html");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to mode_lora.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    bool need_saving = false;
    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "interval") {
        cfg.interval = server.arg(i).toInt();
        need_saving = true;
      }
      else if (server.argName(i) == "sample") {
        measure::sample();
        uint8_t msg[256];
        uint8_t msg_len = mode_lora::fmt_meas(msg);
        lora_meas_cur = "[";
        for (uint8_t i=0; i <msg_len;i++) {
            lora_meas_cur += String(msg[i]);
            lora_meas_cur += ", ";
        }
        lora_meas_cur += "]";
      }
    }
    if (need_saving) {
      config::save_file();
    }
  }
  server.process_and_send("/page_mode_lora.html", html_mode_lora::processor);
}

void html_mode_lora::handle_chg_mode() {
#if (DEBUG == 1)
  Serial.println("handle_chg_mode_lora");
#endif
  cfg_mode = MODE_LORA;
  ESP.deepSleep(0);
}

