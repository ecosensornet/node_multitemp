#pragma once

#include <Arduino.h>

#define LED_SIGNAL 15

// measure
#define PIN_CMD_VEXT_SENSOR 3
#define PIN_CMD_VEXT_LORA 34
#define PIN_D0 1
#define PIN_D1 2
#define PIN_D2 4
#define PIN_D3 36
#define PIN_D4 38
#define PIN_D5 40
#define PIN_D6 39
#define PIN_D7 37
#define PIN_D8 35

// LORA
#define PIN_SPI_SCLK 7  // fixed since SPI.begin() smw in lmic code
#define PIN_SPI_MISO 9  // fixed since SPI.begin() smw in lmic code
#define PIN_SPI_MOSI 11  // fixed since SPI.begin() smw in lmic code
#define PIN_LORA_CS 12
#define PIN_LORA_RST 5
#define PIN_LORA_DIO0 16
#define PIN_LORA_DIO1 18
#define PIN_LORA_DIO2 33

namespace board {

void setup_base();
void setup();
void signal_normal();
void signal_warning(uint8_t);

}  // namespace board
