#pragma once

#include <Arduino.h>

namespace mode_espnow {

union {
    uint32_t val;
    uint8_t bytes[4];
} cvtd;

union {
    float val;
    uint8_t bytes[4];
} cvtf;

uint8_t fmt_meas(uint8_t* msg);

}  // namespace mode_espnow
