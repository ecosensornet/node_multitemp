#include "../settings.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


uint8_t mode_espnow::fmt_meas(uint8_t* msg) {
  uint16_t t_int;

  msg[0] = (uint8_t)IID;

  t_int = (uint16_t)((sample_cur.t0 + 273.15) * 100 + 0.5);
  msg[1] = (uint8_t)(t_int & 0xff);
  msg[2] = (uint8_t)((t_int >> 8) & 0xff);
  t_int = (uint16_t)((sample_cur.t1 + 273.15) * 100 + 0.5);
  msg[3] = (uint8_t)(t_int & 0xff);
  msg[4] = (uint8_t)((t_int >> 8) & 0xff);
  t_int = (uint16_t)((sample_cur.t2 + 273.15) * 100 + 0.5);
  msg[5] = (uint8_t)(t_int & 0xff);
  msg[6] = (uint8_t)((t_int >> 8) & 0xff);

#if (DEBUG == 1)
  Serial.print("cur: (");
  Serial.print(sample_cur.t0);
  Serial.print(", ");
  Serial.print(sample_cur.t1);
  Serial.print(", ");
  Serial.print(sample_cur.t2);
  Serial.println(")");

  Serial.print("[");
  for (uint8_t i = 0; i < 7; i++) {
    Serial.print(msg[i]);
    Serial.print(", ");
  }
  Serial.println("]");
#endif // DEBUG

  return 7;
}

