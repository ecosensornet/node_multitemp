#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

calibration::Calibration calib;


void calibration::load_file() {
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("/calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  for (uint8_t i = 0; i < 8; i++) {
    calib.sensor0[i] = doc["sensor0"]["sid"][i];
    calib.sensor1[i] = doc["sensor1"]["sid"][i];
    calib.sensor2[i] = doc["sensor2"]["sid"][i];
  }

  yield();
}


void calibration::save_file() {
  uint8_t i;
  StaticJsonDocument<384> doc;

  JsonArray sensor0_sid = doc["sensor0"].createNestedArray("sid");
  for (i = 0; i < 8; i++) {
    sensor0_sid.add(calib.sensor0[i]);
  }

  JsonArray sensor1_sid = doc["sensor1"].createNestedArray("sid");
  for (i = 0; i < 8; i++) {
    sensor1_sid.add(calib.sensor1[i]);
  }

  JsonArray sensor2_sid = doc["sensor2"].createNestedArray("sid");
  for (i = 0; i < 8; i++) {
    sensor2_sid.add(calib.sensor2[i]);
  }

  File fhw = LittleFS.open("/calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}
