#include <ArduinoJson.h>
#include <LittleFS.h>

#include "src/board.h"
#include "src/calibration/calibration.h"
#include "src/measure/measure.h"

unsigned long t0;

void fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["t0"] = sample_cur.t0;
  doc["t1"] = sample_cur.t1;
  doc["t2"] = sample_cur.t2;

  serializeJsonPretty(doc, msg);
}

void setup() {
  board::setup();

  calibration::setup();
  measure::setup();
}

void loop() {
  String msg = "";

  t0 = millis();
  measure::sample();
  Serial.println("elapsed: " + String(millis() - t0));

  fmt_meas(msg);
  Serial.println(msg);

  delay(1000);
}
