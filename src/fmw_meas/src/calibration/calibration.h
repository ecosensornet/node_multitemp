#pragma once

#include "calibration_base.h"


namespace calibration {

typedef struct {
  uint8_t sensor0[8];
  uint8_t sensor1[8];
  uint8_t sensor2[8];
} Calibration;

}  // namespace calibration

extern calibration::Calibration calib;
