#pragma once

#include "../board.h"
#include "measure_base.h"

#define PIN_TH_DATA PIN_D0

namespace measure {

typedef struct {
  float t0;
  float t1;
  float t2;
} Sample;

}  // namespace measure

extern measure::Sample sample_cur;
