"""
Node used to measure bunch of temperatures using normal temp probes
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
