"""
Decode message into values
"""
import json
from dataclasses import dataclass


@dataclass
class Sample:
    iid: int = 1
    """Interface id
    """

    t0: float = 0
    """t0 [°C]
    """

    t1: float = 0
    """t1 [°C]
    """

    t2: float = 0
    """t2 [°C]
    """

    def decode(self, buffer):
        if buffer[0] != self.iid:
            raise IndexError("bad interface")

        t0_int = buffer[1] + (buffer[2] << 8)
        self.t0 = t0_int / 100 - 273.15

        t1_int = buffer[3] + (buffer[4] << 8)
        self.t1 = t1_int / 100 - 273.15

        t2_int = buffer[5] + (buffer[6] << 8)
        self.t2 = t2_int / 100 - 273.15

    def to_json(self):
        data = {
            "t0": self.t0,
            "t1": self.t1,
            "t2": self.t2,
        }

        return json.dumps(data, indent=2)
