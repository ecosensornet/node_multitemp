#include "src/board.h"
#include "src/config/config.h"
#include "src/measure/measure.h"
#include "src/mode_lora/mode_lora.h"


unsigned long last_msg_time;

void setup() {
  board::setup();
  config::setup();

  delay(1500);  // When the power is turned on, a delay is required.

  Serial.println("LoRa Node Multitemps");
  mode_lora::setup();

  measure::setup();
  measure::sample();

  last_msg_time = millis();

  while (mode_lora::not_joined()) {
    mode_lora::loop();
  }
}

void loop() {
  if ((millis() - last_msg_time) > 30 * 1e3) {
    Serial.println("do send");
    mode_lora::send_sample();
    last_msg_time = millis();
  }
  Serial.println("loop for nothing");
  delay(10 * 1e3);
}
