function decodeUplink(input) {
  if (input.bytes[0] != 1) {
    return {
      data: {},
      warnings: [],
      errors: ["bad node interface"]
    };
  }

  return {
    data: {
      t0_f: (input.bytes[1]+ (input.bytes[2] << 8)) / 100 - 273.15,  // [bytes] to [°C]
      t1_f: (input.bytes[3]+ (input.bytes[4] << 8)) / 100 - 273.15,  // [bytes] to [°C]
      t2_f: (input.bytes[5]+ (input.bytes[6] << 8)) / 100 - 273.15,  // [bytes] to [°C]
    },
    warnings: [],
    errors: []
  };
}
