#pragma once

#include <Arduino.h>

namespace mode_lora {

enum {
    MSG_PIPED = 1,
    MSG_TRANSMITTED = 2,
};

void setup();
void loop();
bool send_msg(uint8_t* msg, uint8_t nb_bytes);
bool send_sample();
bool not_joined();
bool still_transmitting();

}  // namespace mode_lora
