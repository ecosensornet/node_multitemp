#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>

#include "../board.h"
#include "../settings.h"
#include "../config/config.h"
#include "../measure/measure.h"
#include "fmt_meas.h"

#include "mode_lora.h"

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = PIN_LORA_CS,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = PIN_LORA_RST,
  .dio = { PIN_LORA_DIO0, PIN_LORA_DIO1, PIN_LORA_DIO2 }
};

// from https://jackgruber.github.io/2020-04-13-ESP32-DeepSleep-and-LoraWAN-OTAA-join/
RTC_DATA_ATTR lmic_t RTC_LMIC;
static int join_status = EV_JOINING;
static int msg_status = mode_lora::MSG_TRANSMITTED;
#if (DEBUG == 1)
unsigned long lora_elapsed;
#endif // DEBUG

void os_getArtEui(u1_t *buf) {
  memcpy(buf, cfg.APPEUI, 8);
}

void os_getDevEui(u1_t *buf) {
  memcpy(buf, cfg.DEVEUI, 8);
}

void os_getDevKey(u1_t *buf) {
  memcpy(buf, cfg.APPKEY, 16);
}

void SaveLMICToRTC(int deepsleep_sec) {
    RTC_LMIC = LMIC;
   // EU Like Bands

    //System time is resetted after sleep. So we need to calculate the dutycycle with a resetted system time
    unsigned long now = millis();
    for (int i = 0; i < MAX_BANDS; i++) {
        ostime_t correctedAvail = RTC_LMIC.bands[i].avail - ((now/1000.0 + deepsleep_sec ) * OSTICKS_PER_SEC);
        if(correctedAvail < 0) {
            correctedAvail = 0;
        }
        RTC_LMIC.bands[i].avail = correctedAvail;
    }

    RTC_LMIC.globalDutyAvail = RTC_LMIC.globalDutyAvail - ((now/1000.0 + deepsleep_sec ) * OSTICKS_PER_SEC);
    if (RTC_LMIC.globalDutyAvail < 0) {
        RTC_LMIC.globalDutyAvail = 0;
    }
}

void LoadLMICFromRTC() {
    LMIC = RTC_LMIC;
}

void onEvent(ev_t ev) {
#if (DEBUG == 1)
  Serial.print(os_getTime());
  Serial.print(": ");
#endif // DEBUG
  switch (ev) {
    case EV_TXCOMPLETE:
#if (DEBUG == 1)
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
#endif // DEBUG
      msg_status = mode_lora::MSG_TRANSMITTED;

      if (LMIC.txrxFlags & TXRX_ACK) {
#if (DEBUG == 1)
        Serial.println(F("Received ack"));
#endif // DEBUG
      }

      if (LMIC.dataLen) {
        // data received in rx slot after tx
#if (DEBUG == 1)
        Serial.print(F("Data Received: "));
        // Serial.write(LMIC.frame + LMIC.dataBeg, LMIC.dataLen);
        // Serial.println();
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
#endif // DEBUG
      }
      break;
    case EV_JOINING:
#if (DEBUG == 1)
      Serial.println(F("EV_JOINING: -> Joining..."));
#endif // DEBUG
      join_status = EV_JOINING;
      break;
    case EV_JOIN_FAILED:
#if (DEBUG == 1)
      Serial.println(F("EV_JOIN_FAILED: -> Joining failed"));
#endif // DEBUG
      break;
    case EV_JOINED:
#if (DEBUG == 1)
      Serial.println(F("EV_JOINED"));
#endif // DEBUG
      join_status = EV_JOINED;

      delay(3);
      // Disable link check validation (automatically enabled
      // during join, but not supported by TTN at this time).
      LMIC_setLinkCheckMode(0);

      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
#if (DEBUG == 1)
      Serial.println(F("EV_RXCOMPLETE"));
#endif // DEBUG
      break;
    case EV_LINK_DEAD:
#if (DEBUG == 1)
      Serial.println(F("EV_LINK_DEAD"));
#endif // DEBUG
      break;
    case EV_LINK_ALIVE:
#if (DEBUG == 1)
      Serial.println(F("EV_LINK_ALIVE"));
#endif // DEBUG
      break;
    default:
#if (DEBUG == 1)
      Serial.println(F("Unknown event"));
#endif // DEBUG
      break;
  }
}

void mode_lora::setup() {
#if (DEBUG == 1)
  Serial.println("mode LORA");
#endif // DEBUG
  // turn on LORA board
  digitalWrite(PIN_CMD_VEXT_LORA, LOW);

  // LMIC init
  os_init();

  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
  // Set up the channels used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.

  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);  // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);    // g2-band
  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7, 14);

  // Start job
  Serial.print("RTC LMIC");
  Serial.println(RTC_LMIC.seqnoUp);
  if (RTC_LMIC.seqnoUp != 0) {
    LoadLMICFromRTC();
    join_status = EV_JOINED;
  }
  else {
    LMIC_startJoining();
  }

#if (DEBUG == 1)
  lora_elapsed = millis();
#endif // DEBUG
}


void mode_lora::loop() {
  long sleep_time;
  os_runloop_once();
  if (join_status == EV_JOINING) {
#if (DEBUG == 1)
    if (millis() - lora_elapsed > 3e3) {
        Serial.println("joining");
        lora_elapsed = millis();
    }
#endif // DEBUG
  }
//  else if (join_status == EV_JOINED) {
//    mode_lora::send_sample();
//#if (DEBUG == 1)
//    Serial.print("delay: ");
//    Serial.print(cfg.interval);
//    Serial.println(" [s]");
//#endif // DEBUG
//
//    sleep_time = cfg.interval * 1e3 - millis();
//#if (DEBUG == 1)
//    sleep_time -= 300;
//    Serial.print("sleep_time: ");
//    Serial.print(sleep_time);
//    Serial.println(" [ms]");
//    delay(300);  // needed for serial to send msg
//#endif // DEBUG
//    SaveLMICToRTC(cfg.interval);  // (int)(sleep_time / 1e3)
//    // turn off LORA board
//    digitalWrite(PIN_CMD_VEXT_LORA, HIGH);
//
//    if (sleep_time > 0) {
//      ESP.deepSleep(sleep_time * 1e3);
//    }
//    else {
//#if (DEBUG == 1)
//      Serial.println("restart");
//      delay(300);  // needed for serial to send msg
//#endif // DEBUG
//      ESP.deepSleep(0);
//    }
//  }
}

bool mode_lora::send_msg(uint8_t* msg, uint8_t nb_bytes){
  if (join_status == EV_JOINING) {
#if (DEBUG == 1)
    Serial.println(F("Not joined yet"));
#endif // DEBUG
    return false;
  } else if (LMIC.opmode & OP_TXRXPEND) {
#if (DEBUG == 1)
    Serial.println(F("OP_TXRXPEND, not sending"));
#endif // DEBUG
    return false;
  } else {
#if (DEBUG == 1)
    Serial.println(F("OP_TXRXPEND, sending ..."));
#endif // DEBUG
    msg_status = mode_lora::MSG_PIPED;
    LMIC_setTxData2(1, msg, nb_bytes, 0);

    while (mode_lora::still_transmitting()) {
      os_runloop_once();
    }
    return true;
  }
}

bool mode_lora::send_sample() {
  // perform measure
  measure::sample();

  // send message
  uint8_t msg[256];
  uint8_t msg_len = mode_lora::fmt_meas(msg);
  return mode_lora::send_msg(msg, msg_len);
}

bool mode_lora::not_joined() {
    return join_status != EV_JOINED;
}


bool mode_lora::still_transmitting() {
    return msg_status != mode_lora::MSG_TRANSMITTED;
}

