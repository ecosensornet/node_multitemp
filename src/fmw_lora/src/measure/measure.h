#pragma once

namespace measure {

typedef struct {
  float t0;
  float t1;
  float t2;
} Sample;

void setup();
void sample();

}  // namespace measure

extern measure::Sample sample_cur;
