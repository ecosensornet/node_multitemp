#pragma once

#include <Arduino.h>

#define MODE_CONFIG 0
#define MODE_ESPNOW 1
#define MODE_LORA 2

typedef struct {
  char node_id[13];

  uint16_t interval;  // [s] time between two measures

  // local espnow settings
  uint8_t gate_ip[6];  // MAC address of gateway

  // lora settings
  uint8_t APPEUI[8];  // Chose LSB mode on the console
  uint8_t DEVEUI[8];  // Chose LSB mode on the console
  uint8_t APPKEY[16];  // Chose MSB mode on the console

} NodeConfig;

extern uint8_t cfg_mode;
extern NodeConfig cfg;

namespace config {

void setup();
void load_file();
void save_file();

void fetch_node_id(char*);

}
