#include <ArduinoJson.h>
#include <WiFi.h>
#include <LittleFS.h>

#include "../settings.h"

#include "config.h"

RTC_DATA_ATTR uint8_t cfg_mode = MODE_CONFIG;
NodeConfig cfg;


void config::setup() {
  load_file();
  fetch_node_id(cfg.node_id);
}


void config::load_file() {
  uint8_t i;
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("/config.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif // DEBUG

  cfg.interval = doc["measure"]["interval"];
  for (i = 0; i < 6; i++) {
    cfg.gate_ip[i] = doc["mode_espnow"]["gateway"][i];
  }
  for (i = 0; i < 8; i++) {
    cfg.APPEUI[i] = doc["mode_lora"]["APPEUI"][i];
  }
  for (i = 0; i < 8; i++) {
    cfg.DEVEUI[i] = doc["mode_lora"]["DEVEUI"][i];
  }
  for (i = 0; i < 16; i++) {
    cfg.APPKEY[i] = doc["mode_lora"]["APPKEY"][i];
  }

  yield();
}


void config::save_file() {
  uint8_t i;
  StaticJsonDocument<384> doc;

  doc["measure"]["interval"] = cfg.interval;

  JsonArray mode_espnow_gateway = doc["mode_espnow"].createNestedArray("gateway");
  for (i = 0; i < 6; i++) {
    mode_espnow_gateway.add(cfg.gate_ip[i]);
  }
  JsonArray mode_lora_APPEUI = doc["mode_lora"].createNestedArray("APPEUI");
  for (i = 0; i < 8; i++) {
    mode_lora_APPEUI.add(cfg.APPEUI[i]);
  }
  JsonArray mode_lora_DEVEUI = doc["mode_lora"].createNestedArray("DEVEUI");
  for (i = 0; i < 8; i++) {
    mode_lora_DEVEUI.add(cfg.DEVEUI[i]);
  }
  JsonArray mode_lora_APPKEY = doc["mode_lora"].createNestedArray("APPKEY");
  for (i = 0; i < 16; i++) {
    mode_lora_APPKEY.add(cfg.APPKEY[i]);
  }

  File fhw = LittleFS.open("/config.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif // DEBUG

  yield();
}


void config::fetch_node_id(char* buffer) {
  uint8_t maca[6];
  WiFi.macAddress(maca);

  sprintf(buffer,
          "%02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
         );
}
