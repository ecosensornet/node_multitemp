========================
node_multitemp
========================

.. {# pkglts, doc

.. image:: https://ecosensornet.gitlab.io/node_multitemp/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/node_multitemp/0.0.1/

.. image:: https://ecosensornet.gitlab.io/node_multitemp/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/node_multitemp

.. image:: https://ecosensornet.gitlab.io/node_multitemp/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/node_multitemp/

.. image:: https://badge.fury.io/py/node_multitemp.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/node_multitemp

.. #}
.. {# pkglts, glabpkg_dev, after doc

.. #}

Node used to measure bunch of temperatures using normal temp probes

